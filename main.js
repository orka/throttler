define(['p!-throttler/network',
    'p!-throttler/error',
    'p!-throttler/throttler',
    'p!-throttler/priority',
    'p!-throttler/hub'
], function($network, $error, $throttler, $priority, $hub) {
    'use strict';
    return {
        network: $network,
        error: $error,
        throttler: $throttler,
        priority: $priority,
        hub: $hub
    };
});
