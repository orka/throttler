define(['p!-defineclass', 'p!-assertful'], function($classify, $assertful) {
    'use strict';
    //defined all and any view type that we'll handle by $controller
    //all defenitions must match configuration, else applications wll not run
    var defaultCollection = [];
    var Priority;

    function sortPriorities (__a, __b) {
        return __a.level - __b.level;
    }

    Priority = {
        constructor: function(__level, __subPriority, __type, __collection) {
            if (!$assertful.int(__level) || !$assertful.optObject(__subPriority) || !$assertful.array(__collection)) {
                throw 'Invalid arguments';
            }
            if (__subPriority && !Priority.isOffspring(__subPriority)) {
                throw 'sub-priorities must by an instance of "Priority"';
            }
            this.superLevel = __level;
            this.subPriority = __subPriority;
            this.level = this.superLevel + (__subPriority ? parseFloat('0.' + this.subPriority.level) : 0);
            this.type = __type;
            this.collection = __collection;
            this.collection.push(this);
            this.collection.sort(sortPriorities);
        },
        deconstructor: function() {
            this.collection = null;
            this.subPriority = null;
        },
        getLevel: function() {
            return this.level;
        },
        getTop: function() {
            return this.collection[0];
        },
        getLast: function() {
            return this.collection[this.collection.length - 1];
        },
        getNext: function () {
            var _index = this.collection.indexOf(this);
            return this.collection[_index + 1];
        },
        gePrev: function () {
            var _index = this.collection.indexOf(this);
            return this.collection[_index - 1];
        }
    };
    //define as class
    Priority = $classify('Priority', Priority);

    Priority.TOP = Priority(0, null, 'TOP', defaultCollection);
    //this priority puts controller before  PRIORITY_MEDIUM
    Priority.HIGH = Priority(1, null, 'HIGH', defaultCollection);
    //this priority puts controller before  PRIORITY_LOW
    Priority.MEDIUM = Priority(2, null, 'MEDIUM', defaultCollection);
    //this priority puts controller before PRIORITY_BOTTOM
    Priority.LOW = Priority(3, null, 'LOW', defaultCollection);
    //this priority puts controller at the end of defaultCollection
    Priority.BOTTOM = Priority(4, null, 'BOTTOM', defaultCollection);

    Priority.DEFAULT_PRIORITY = Priority.LOW;

    return Priority;
});
