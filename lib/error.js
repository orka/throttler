/**
 * Common error types definitions
 */
define(['p!-error'], function($error) {
    'use strict';

    var type = {
        MAX_QUEUE: 'MAX_QUEUE'
    };

    var ErrorClass = $error.subclass('CacheableError', {
        constructor: function(__type, __message) {
            ErrorClass.super.constructor.apply(this, arguments);
        },
        deconstructor: function() {
            ErrorClass.super.deconstructor.apply(this, arguments);
        },
        TYPE: type
    });

    ErrorClass.TYPE = type;
    return Object.freeze(ErrorClass);
});
