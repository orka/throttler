define(['p!-eventful',
    'p!-assertful',
    'p!-throttler/error',
    'p!-logger'
], function($eventful,
    $assertful,
    $error,
    $logger) {
    'use strict';
    var queueSize = 100; //default queue size
    var threads = 1; //default number of threads

    var Throttler = {
        /**
         * Class constructor
         * @param   {Object} __bus       Array of current threads shared by all type throttlers
         * @param   {Number} __threads   Number of threads allowed
         * @param   {Number} __queueSize Maximum number of imtes to be queued, beyond which a tail of array will be cat to this number
         * @param   {Object} __priority  Order of the throttler instance to aid in processing of a central hub
         */
        constructor: function(__bus, __threads, __queueSize, __priority) {
            Throttler.super.constructor.call(this);
            //set boounded event handler (mus bount to successfully removed the handler)
            this.__onDoneBound__ = this.__onDone__.bind(this);
            this.__queue__ = [];
            //if __queueSize is not defined - set default
            this.__max = $assertful.optPint(__queueSize) ? __queueSize : queueSize;
            this.priority = __priority;
            this.__bus = __bus;
            this.threads = __threads || threads;
        },

        /**
         * Dereferences all current itemes in queue and locat variables
         */
        deconstructor: function() {
            Throttler.super.deconstructor.call(this);
            this.removeAll();
            this.__queue__ = null;
            this.__max = null;
            this.__onDoneBound__ = null;
            this.nextItem = null;
            this.__bus = null;
        },

        verbose: function() {
            // $logger.setTags($logger.TAGS.NETWORK);
            $logger.verbose.apply($logger, [Throttler.name].concat(Array.prototype.slice.call(arguments, 0)).concat([$logger.TAGS.NETWORK]));
        },

        log: function() {
            // $logger.setTags($logger.TAGS.NETWORK);
            $logger.log.apply($logger, [Throttler.name].concat(Array.prototype.slice.call(arguments, 0)).concat([$logger.TAGS.NETWORK]));
        },

        /**
         * Prepends new item to the begining of the queue regardless if it was already added in the queue
         * If item was founf to be in the queue already - removes it from the current position and places it to the beginning of queue
         * @param {Boolean} __item object to be throttled
         * @returns {Boolean} true if NEW item added, false if already existed
         */
        add: function(__item) {
            var _index;
            if (__item.__addedToThrottler__) {
                return false;
            }

            _index = this.__queue__.indexOf(__item);
            if (_index === 0) {
                // exists and is the first item
                this.emit('updateThrottler');
                return false;
            }

            if (this.__queue__.length > 1 && _index > 0) {
                //already exist but not the first, splice it
                this.__queue__.splice(_index, 1);
                this.__queue__.unshift(__item);
                return false;
            }

            __item.addListener('deconstructed', this.__onDoneBound__);
            //at to the BEGINNING of queue!
            this.__queue__.unshift(__item);
            __item.__addedToThrottler__ = true;
            //cap to max
            if (this.__queue__.length > this.__max) {
                this.__queue__.splice(this.__max, this.__queue__.length);
                this.verbose('Queue exceeded max (' + this.__max + ')! Tail was cut!');
            }
            this.verbose('New Item Idded!', 'Length: ' + this.__queue__.length);
            //let the subclasses know to set listeners
            this.emit('added', __item);
            this.emit('updateThrottler');
            return true;
        },

        /**
         * Generally called by an external hub or manually
         * Takes first item in queue and proccesses it, but only if the bus is not busy
         * @returns {Boolean} true if updated
         */
        update: function() {
            if (this.__bus.length >= this.threads) {
                this.verbose('Exceeded bus max, passing on update');
                return false;
            }

            if (!this.setNext() || this.__bus.indexOf(this.nextItem) >= 0) {
                this.verbose('No new item to process', 'bus: ' + this.__bus.length, ', queue: ' + this.__queue__.length);
                return false;
            }
            //add to bus and trigger loaf
            this.__bus.push(this.nextItem);
            //let the owner know to proccess next item
            this.emit('doNext', this.nextItem);
            //succeeded
            return true;
        },

        /**
         * Takes first item in queue and proccesses it to but - if the bus is not busy
         * @returns {Boolean} true if updated
         */
        setNext: function() {
            this.nextItem = null;
            //if no queue or bus is full
            if (this.__queue__.length <= 0) {
                //failed
                return false;
            }
            //add to the beginning first in queue
            this.nextItem = this.__queue__.shift();
            //
            this.verbose('Proccessing nextItem', 'bus: ' + this.__bus.length, ', queue: ' + this.__queue__.length);
            //succeeded
            return true;
        },

        /**
         * Deconstructed imte event listener
         * Also used for various other purposes, see network
         * @param   {Object} __item throttleed item
         */
        __onDone__: function(__item) {
            this.remove(__item);
        },

        /**
         * Removes currently refferenced throttled item as well as event listeners assosiated with
         * This throttler instance
         * @param   {Object} __item throtteled item
         */
        remove: function(__item) {
            var _index = this.__bus.indexOf(__item);

            if (_index < 0) {
                this.log('loaded item is no longer in main bus!', __item);
            } else {
                //remove loaded
                this.__bus.splice(_index, 1);
            }

            //remove listeners
            __item.removeListener('deconstructed', this.__onDoneBound__);
            this.verbose('Item removed', 'bus: ' + this.__bus.length, ', queue: ' + this.__queue__.length);
            this.emit('removed', __item);
            this.emit('updateThrottler');
        },

        /**
         * Clear all items from the queue (sets length to 0)
         */
        removeAll: function() {
            this.__queue__.forEach(function (__item) {
                __item.__addedToThrottler__ = false;
            });
            this.__queue__.length = 0;
        }
    };

    Throttler = $eventful.emitter.subclass('Throttler', Throttler);
    return Throttler;
});
