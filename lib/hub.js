define(['p!-eventful', 'p!-assertful', 'p!-throttler/error', 'p!-logger'], function($eventful, $assertful, $error, $logger) {
    'use strict';
    var Hub;

    function prioritySort(__a, __b) {
        return __a.priority.getLevel() - __b.priority.getLevel();
    }

    Hub = {
        /**
         * Hub controlles all a like throttler priority proccessing
         * @param {Object} __typeClass A Class Object that is used to create all incoming throttler instances
         * to ansure the type is matched with the nature of throttled pipe
         */
        constructor: function(__typeClass) {
            Hub.super.constructor.call(this);
            this.__throttlers__ = [];
            this.typeClass = __typeClass;
            this.__updateBound__ = this.update.bind(this);
        },

        deconstructor: function() {
            Hub.super.deconstructor.call(this);
            this.__throttlers__ = null;
        },

        /**
         * Adds throttler instance (of same type) to a local colletction
         * of throttler to be processed on update event
         * @param {Object} __throttler throttler instance
         */
        add: function(__throttler) {
            if (!this.typeClass.isOffspring(__throttler)) {
                throw 'not a throttler type';
            }
            this.__throttlers__.push(__throttler);
            //sort immdiatly
            this.__throttlers__.sort(prioritySort);

            __throttler.addListener('update', this.__updateBound__);
        },

        /**
         * Removes throttler instance from a local array of throttlers
         * @param   {Object} __throttler instance of throttler class
         */
        remove: function(__throttler) {
            var _index = this.__throttlers__.indexOf(__throttler);
            if (_index >= 0) {
                this.__throttlers__.splice(_index, 1)[0].removeListener('update', this.__updateBound__);
            }
        },

        /**
         * Calls .update() method on each throttler in collection based on priority order
         */
        update: function() {
            this.__throttlers__.forEach(function(__throttler) {
                __throttler.update();
            });
        }
    };

    Hub = $eventful.emitter.subclass('ThrottlerHub', Hub);
    return Hub;
});
