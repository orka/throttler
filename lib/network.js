define(['p!-throttler/throttler', 'p!-throttler/hub', 'p!-image/error'], function($throttler, $hub, $error) {
    'use strict';
    var bus = [];// main bus which will hold all currently in proggess items such as loading etc...
    var threads = 6;//max amount of threads
    var queueSize = 100;//a default queue size
    var Network;//class handle
    //define hub to constroll collection of throttlers
    var hub;

    Network = {
        /**
         * Class constructor
         * Creates instance of a the Network type throttler that uses image Class (and simmilar classes)
         * @param   {Number} __max      max queue lenth beyond which array is cut
         * @param   {Object} __priority order of this instance to be processed by a centrall hub
         */
        constructor: function(__max, __priority) {
            Network.super.constructor.call(this, bus, threads, __max || queueSize, __priority);
            this.__onErrorBound__ = this.__onError__.bind(this);
            //add listeners
            this.addListener('added', this.__onAdded__.bind(this));
            this.addListener('removed', this.__onRemoved__.bind(this));
            this.addListener('doNext', this.__onDoNext__.bind(this));
            this.addListener('updateThrottler', this.__onUpdateThrottler__.bind(this));
            //ad to hub fo macromanagment
            hub.add(this);
        },

        deconstructor: function() {
            Network.super.deconstructor.call(this);
            this.__onErrorBound__ = null;
            hub.remove(this);
        },

        __onUpdateThrottler__: function () {
            hub.update();
        },

        /**
         * When Item is added attaches network event listeneres
         * @param   {Object} __this instance
         * @param   {Object} __item Item added (Ex: Image)
         */
        __onAdded__: function(__this, __item) {
            //set listeners for network object
            __item.addListener('loaded', this.__onDoneBound__);
            __item.addListener('error', this.__onErrorBound__);
            __item.addListener('stopped', this.__onDoneBound__);
        },

        /**
         * Custom Network error
         * @param {Object} __item caller item
         * @param {Object} __error type error
         */
        __onError__: function(__item, __error) {
            switch (__error.type) {
                case $error.TYPE.LOAD:
                    this.__onDone__(__item);
                    break;
            }
        },

        /**
         * When Item is removed detaches network events listeneres
         * @param   {Object} __this instance
         * @param   {Object} __item Item added (Ex: Image)
         */
        __onRemoved__: function(__this, __item) {
            //remove listeners
            __item.removeListener('loaded', this.__onDoneBound__);
            __item.removeListener('error', this.__onErrorBound__);
            __item.removeListener('stopped', this.__onDoneBound__);
        },

        /**
         * Event listener/handler to handle network objects
         */
        __onDoNext__: function() {
            this.nextItem.load(null, true);
        }
    };

    Network = $throttler.subclass('NetworkThrottler', Network);
    hub = $hub(Network);
    return Network;
});
